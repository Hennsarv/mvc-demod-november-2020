﻿using NorthwindMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NorthwindMVC.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            return View(Inimene.Rahvas.Values);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int? id)
        {
            if (!Inimene.Rahvas.ContainsKey(id ?? 0))
                //return HttpNotFound(); 
                return RedirectToAction("Index");
            else
                return View(Inimene.Rahvas[id.Value]);


        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {

            try
            {
                // TODO: Add insert logic here
                new Inimene { Nimi = collection["Nimi"], Vanus = int.Parse(collection["Vanus"]) };

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int? id)
        {
            // return View();
            if (Inimene.Rahvas.ContainsKey(id ?? 0))

                Inimene.Rahvas.Remove(id.Value);
            return RedirectToAction("Index");


        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
