﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NorthwindMVC.Models
{
    public class Inimene
    {
        public static Dictionary<int, Inimene> Rahvas = new Dictionary<int, Inimene>();

        static Inimene()
        {
            new Inimene { Nimi = "Henn", Vanus = 65 };
            new Inimene { Nimi = "Ants", Vanus = 44 };
            new Inimene { Nimi = "Peeter", Vanus = 22 };
        }

            
        static int nr = 0;

        public int Nr = ++nr;
        [Display(Name = "Inimese nimi")] // attribuut
        [Required(AllowEmptyStrings= false, ErrorMessage="nimi peab olema")]
        public string Nimi { get; set; }
        [Range(18,99, ErrorMessage = "Vanus vahemikus 18-99")]
        public int Vanus { get; set; }

        public Inimene() => Rahvas.Add(Nr, this);


        

    }
}