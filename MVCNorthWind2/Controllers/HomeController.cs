﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCNorthWind2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Minu rakenduse leht";

            return View();
        }
        [HttpPost]
        public ActionResult About(string Name, int? Age)
        {
            ViewBag.Message = "Minu rakenduse post leht";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Meie kontaktid";

            return View();
        }
    }
}