﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCNorthWind2.Models;
using System.ComponentModel.DataAnnotations;


namespace MVCNorthWind2.Models
{
    [MetadataType(typeof(EmployeeMetadata))]
    partial class Employee
    {
        public string FullName => FirstName + " " + LastName;
    }

    public class EmployeeMetadata
    { 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? BirthDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? HireDate { get; set; }

    }
}

namespace MVCNorthWind2.Controllers
{
    public class EmployeesController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: Employees
        public ActionResult Index(int id = 0)
        {
            ViewBag.employeeId = id;

            var employees = db.Employees
                .Where(x => x.ReportsTo == id || id == 0)
                
                .Include(e => e.Manager);
            return View(employees.ToList());
        }

        public ActionResult Photo(int? id)
        {
            Employee employee = db.Employees.Find(id ?? 0);
            if ((employee?.Photo?.Length ?? 0) == 0) return HttpNotFound();

            return File(
                employee.Photo[0] == 21 ? 
                employee.Photo.Skip(78).ToArray() : 
                employee.Photo, 
                "image/jpg");

        }


        // GET: Employees/Details/5
        public ActionResult Details(int? id, int alluvatega = 1)
        {
            ViewBag.alluvatega = alluvatega;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName");
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo,PhotoPath")] Employee employee)
        {
            int olemas = db.Employees.Where(x => (x.FirstName + " " + x.LastName) == employee.FullName).Count();
            if (olemas > 0)
            {
                ModelState.AddModelError("FirstName", "sellise nimega on keegi juba");
                ModelState.AddModelError("LastName", "sellise nimega on keegi juba");
            }

            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "EmployeeID,LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Notes,ReportsTo")] 
            Employee employee, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.Entry(employee).Property("Photo").IsModified = false;

                if((file?.ContentLength??0) > 0)
                {
                    using(System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        employee.Photo = br.ReadBytes(file.ContentLength);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ReportsTo = new SelectList(db.Employees, "EmployeeID", "FullName", employee.ReportsTo);
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
