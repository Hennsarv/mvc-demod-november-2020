﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCNorthWind2.Models;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Data.Entity.Validation;

namespace MVCNorthWind2.Models
{
    [MetadataType(typeof(ProductMetadata))]
    partial class Product
    {
        public decimal Laoseis => (UnitPrice ?? 0M) * (UnitsInStock ?? 0);
    }

    public class ProductMetadata
    {
        [Display(Name = "Money in stock")]
        [DisplayFormat(DataFormatString = "{0:$0.00}")]
        public decimal Laoseis { get; }

        [Range(1, 200, ErrorMessage = "hind peab olema vahemikus 5 kuni 200")] // validation
        public decimal? UnitPrice { get; set; }
    
    }
}


namespace MVCNorthWind2.Controllers
{
    public class ProductsController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        
        

        // GET: Products
        public ActionResult Index
            (
                int size = 0,       // lehe suurus - kui 0, siis kõik
                int page = 1,       // lehe number
                int id = 0,         // CategoryID-filter (kui 0 siis kõik)
                string sort = "ProductId",      // sorteerimisveerg: name, price, units
                string direction = "asc",        // surteerimissuund: asc, desc
                string liiguta = "",    // kas järgmine või eelmine leht
                int editId = 0         // millise toote hinda muuta
            )
        {
            page += (liiguta == "\u2b05" ? -1 : liiguta == "\u27a1" ? 1 : 0);
            
            ViewBag.sort = sort;
            ViewBag.direction = direction;
            ViewBag.size = size;
            ViewBag.page = page;
            ViewBag.editId = editId;
            ViewBag.categoryId = id;


            var products = db.Products
                .Where(x => x.CategoryID == id || id == 0) // see rida
                // ühe tingimusega ja ühe lausega saame 0 korral kõik ja n korral need
                //.Include(p => p.Category)
                ;

            var sortedProduct = products.OrderBy(x => x.ProductID);
            switch((sort+direction).ToLower())
            {
                case "nameasc":
                    sortedProduct = products.OrderBy(x => x.ProductName);
                    break;
                case "namedesc":
                    sortedProduct = products.OrderByDescending(x => x.ProductName);
                    break;
                case "priceasc":
                    sortedProduct = products.OrderBy(x => x.UnitPrice);
                    break;
                case "pricedesc":
                    sortedProduct = products.OrderByDescending(x => x.UnitPrice);
                    break;
                case "unitsasc":
                    sortedProduct = products.OrderBy(x => x.UnitsInStock);
                    break;
                case "unitsdesc":
                    sortedProduct = products.OrderByDescending(x => x.UnitsInStock);
                    break;
            }
            ViewBag.pages = size == 0 ? 0 : sortedProduct.Count() / size;
            if (size == 0) 
                return View(sortedProduct.ToList());
            else 
                return View(sortedProduct.Skip((page - 1) * size).Take(size).ToList());
            // selle saab ka lühemini
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            //if (id == null)
            //{
            //    //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //    return RedirectToAction("Index");
            //}
            Product product = db.Products.Find(id??0);
            if (product == null)
            {
                //return HttpNotFound();
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Create
      
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", 7);
            return View(new Product { UnitPrice = 10, CategoryID = 7});
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued,State")] Product product)
        {
            var op = db.Products.Where(x => x.ProductName == product.ProductName).Count();
            if(op > 0)
            {
                ModelState.AddModelError("ProductName", "Sellise nimega toode meil juba on");
            }

            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued,State")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult EditPrice(
                int productId = 0,
                int categoryId = 0,
                decimal UnitPrice = 0,
                int size=0,
                int page=0,
                string sort="",
                string direction=""

            )
        {
            var prod = db.Products.Find(productId);
            if (prod != null)
            {
                try
                {
                    prod.UnitPrice = UnitPrice;
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    TempData["error"] = e.Message + ": "  + 
                        string.Join(",",
                            e.EntityValidationErrors
                            .Select(x => string.Join(", ", x.ValidationErrors.Select(y => y.ErrorMessage)))) ;
                }
            }
            return RedirectToAction("Index", new { id = categoryId, size=size,page=page,sort=sort,direction=direction });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
